FROM ruby:2.3.0
ENV APP_ROOT /usr/src/app

RUN apt-get update && apt-get install -y nodejs --no-install-recommends && rm -rf /var/lib/apt/lists/*

RUN mkdir -p $APP_ROOT
WORKDIR $APP_ROOT

COPY Gemfile $APP_ROOT/
COPY Gemfile.lock $APP_ROOT/

RUN \
  bundle config --local build.nokogiri --use-system-libraries && \
  bundle config --local without test development doc && \
  bundle install -j4

